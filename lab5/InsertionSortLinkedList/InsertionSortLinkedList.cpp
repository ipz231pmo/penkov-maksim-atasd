﻿#include <iostream>
#include <list>
#include <random>
#include <ctime>
#include <chrono>

#define SIZE 10

void Sort(std::list<int>& list, bool grow = true) {
    auto start = std::chrono::high_resolution_clock::now();

    auto i = list.begin()++;
    for (; i != list.end(); i++) {
        for (auto j = i; j != list.begin(); j--) {
            auto it_1 = j;
            auto it_2 = j;
            it_2--;
            if (*it_1 > *it_2) break;
            int tmp = *it_1;
            *it_1 = *it_2;
            *it_2 = tmp;
        }
    }
    if (!grow) list.reverse();

    auto end = std::chrono::high_resolution_clock::now();
    int res = std::chrono::duration_cast<std::chrono::microseconds>(end - start).count();
#ifdef _DEBUG
    std::cout << "Sorting time(in nano seconds): ";
#endif // _DEBUG

    std::cout << res << "\n";
}


int main()
{
    std::srand(std::time(0));    
    std::list<int> list;
    for(int i = 0; i < SIZE; i++)
        list.push_back(rand()%1000);
#ifdef _DEBUG
    std::cout << "List:\n";
    for (auto it = list.begin(); it != list.end(); it++) {
        printf("%3d ", *it);
    }
    std::cout << "\n";
#endif // !_DEBUG
    Sort(list);
#ifdef _DEBUG
    std::cout << "List:\n";
        for (auto it = list.begin(); it != list.end(); it++) {
            printf("%3d ", *it);
        }
        std::cout << "\n";
#endif // !_DEBUG    
}
