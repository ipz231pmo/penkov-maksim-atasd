﻿#include <iostream>
#include <chrono>
#define SIZE 10

void Sort(int* arr, int size, bool grow = true) {
    auto start = std::chrono::high_resolution_clock::now();

    for (int i = 1; i < size; i++) {
        for (int j = i; j >= 1; j--) {
            if (arr[j] > arr[j - 1]) break;
            int tmp = arr[j];
            arr[j] = arr[j - 1];
            arr[j - 1] = tmp;
        }

    }

    if (!grow) {
        for (int i = 0; i < size / 2; i++) {
            int tmp = arr[i];
            arr[i] = arr[size - 1 - i];
            arr[size - 1 - i] = tmp;
        }
    }
    auto end = std::chrono::high_resolution_clock::now();
    int res = std::chrono::duration_cast<std::chrono::microseconds>(end - start).count();
#ifdef _DEBUG
    std::cout << "Sorting time(in nano seconds): ";
#endif // _DEBUG

    std::cout << res << "\n";
}

    int arr[SIZE];
int main()
{
    std::srand(std::time(0));
    for (int i = 0; i < SIZE; i++)
        arr[i] = std::rand() % 1000;
#ifdef _DEBUG
    std::cout << "array: ";
    for (int i = 0; i < SIZE; i++) {
        printf("%3d ", arr[i]);
    }
    std::cout << "\n";
#endif // _DEBUG
    Sort(arr, SIZE);
#ifdef _DEBUG
    std::cout << "array: ";
    for (int i = 0; i < SIZE; i++) {
        printf("%3d ", arr[i]);
    }
    std::cout << "\n";
#endif // _DEBUG
}