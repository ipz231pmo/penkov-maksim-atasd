﻿#include <iostream>
#include <list>
#include <chrono>

#define SIZE 10

void Sort(std::list<int>& arr, bool Grow = true) {
    auto start = std::chrono::high_resolution_clock::now();

    for (auto i = arr.begin(); i != arr.end(); i++) {
        auto max = i;
        for (auto j = i; j != arr.end(); j++) {
            if (*j > *max) max = j;
        }
        if (*i != *max) {
            int tmp = *i;
            *i = *max;
            *max = tmp;
        }
    }
    if (Grow) arr.reverse();

    auto end = std::chrono::high_resolution_clock::now();
    int res = std::chrono::duration_cast<std::chrono::microseconds>(end - start).count();
#ifdef _DEBUG
    std::cout << "Sorting time(in nano seconds): ";
#endif // _DEBUG

    std::cout << res << "\n";
}

int main()
{
    std::srand(std::chrono::steady_clock::now().time_since_epoch().count());
    std::list<int> arr;
    for(int i = 0; i < SIZE; i++)
        arr.push_back(std::rand()%1000);

#ifdef _DEBUG
    std::cout << "List:\n";
    for (auto it = arr.begin(); it != arr.end(); it++) {
        printf("%d ", *it);
    }
    std::cout << "\n";
#endif // _DEBUG
    
    Sort(arr, false);

#ifdef _DEBUG
    std::cout << "List:\n";
    for (auto it = arr.begin(); it != arr.end(); it++) {
       printf("%d ", *it);
    }
    std::cout << "\n";
#endif // _DEBUG
}
