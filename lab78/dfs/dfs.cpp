﻿#include <iostream>
#include <vector>
#include <stack>
using namespace std;
bool dfs(vector<vector<int> >& graph, int start, int end) {
    vector<bool> visited(graph.size());
    stack<int> vertices;
    vertices.push(start);
    while (vertices.empty() == false)
    {
        int curr = vertices.top();
        vertices.pop();
        visited[curr] = true;
        if (curr == end) { return true; }

        for (int i = 0; i < graph.size(); i++) {
            if (visited[i] == false && graph[curr][i] == 1)
                vertices.push(i);
        }
    }
    return false;
}
int main()
{
    vector<vector<int> > graph = {
        {0, 1, 1, 0, 0, 0},
        {1, 0, 1, 1, 0, 0},
        {1, 1, 0, 0, 1, 0},
        {0, 1, 0, 0, 1, 0},
        {0, 0, 1, 1, 0, 0},
        {0, 0, 0, 0, 1, 0},
    };
    std::cout << "5 - 4" << (dfs(graph, 5, 4) ? ": Path exists" : ": Path doesn`t exist") << "\n";
    std::cout << "4 - 5" << (dfs(graph, 4, 5) ? ": Path exists" : ": Path doesn`t exist") << "\n";
    std::cout << "0 - 5" << (dfs(graph, 0, 5) ? ": Path exists" : ": Path doesn`t exist") << "\n";
    std::cout << "5 - 1" << (dfs(graph, 5, 1) ? ": Path exists" : ": Path doesn`t exist") << "\n";
}