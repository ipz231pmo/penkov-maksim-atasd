﻿#include <iostream>
union SignedShortInt{
	signed short num;
	struct {
		unsigned short num : 15;
		unsigned short sign : 1;
	} struc;
};

int main()
{
	SignedShortInt test;
	std::cout << "Enter a signed num(16 bit): ";
	std::cin >> test.num;

	std::cout << "1. Using structures & unions\n";
	std::cout << "sign: " << (test.struc.sign == 0 ? "+" : "-") << "\n";
	std::cout << "value: ";
	if (test.struc.sign)
		std::cout << 32768 - test.struc.num << "\n";
	else
		std::cout << test.struc.num << "\n";

	std::cout << "2. Using Bitwise operations\n";
	bool negative = test.num >> 15 == -1;
	signed short value = negative ? ~(test.num-1) : test.num;
	std::cout << "sign: " << (negative ? "-" : "+") << "\nvalue: " << value <<"\n";
}
