﻿#include <iostream>
#define VAR(name, num) name##num
int main()
{
    union {
        float flValue;
        uint32_t uValue;
        struct {
            int32_t sign : 1;
            int32_t info : 8;
            int32_t mantis : 23;
        } stValue;
        struct {
            uint32_t a01 : 1;
            uint32_t a02 : 1;
            uint32_t a03 : 1;
            uint32_t a04 : 1;
            uint32_t a05 : 1;
            uint32_t a06 : 1;
            uint32_t a07 : 1;
            uint32_t a08 : 1;
            uint32_t a09 : 1;
            uint32_t a10 : 1;
            uint32_t a11 : 1;
            uint32_t a12 : 1;
            uint32_t a13 : 1;
            uint32_t a14 : 1;
            uint32_t a15 : 1;
            uint32_t a16 : 1;
            uint32_t a17 : 1;
            uint32_t a18 : 1;
            uint32_t a19 : 1;
            uint32_t a20 : 1;
            uint32_t a21 : 1;
            uint32_t a22 : 1;
            uint32_t a23 : 1;
            uint32_t a24 : 1;
            uint32_t a25 : 1;
            uint32_t a26 : 1;
            uint32_t a27 : 1;
            uint32_t a28 : 1;
            uint32_t a29 : 1;
            uint32_t a30 : 1;
            uint32_t a31 : 1;
            uint32_t a32 : 1;
        } bits;
    } floatNum;

    std::cout << "Enter float number: ";
    std::cin >> floatNum.flValue;

    int bits[32];
    
    bits[0] = floatNum.bits.a01;
    bits[1] = floatNum.bits.a02;
    bits[2] = floatNum.bits.a03;
    bits[3] = floatNum.bits.a04;
    bits[4] = floatNum.bits.a05;
    bits[5] = floatNum.bits.a06;
    bits[6] = floatNum.bits.a07;
    bits[7] = floatNum.bits.a08;

    bits[8] = floatNum.bits.a09;
    bits[9] = floatNum.bits.a10;
    bits[10] = floatNum.bits.a11;
    bits[11] = floatNum.bits.a12;
    bits[12] = floatNum.bits.a13;
    bits[13] = floatNum.bits.a14;
    bits[14] = floatNum.bits.a15;
    bits[15] = floatNum.bits.a16;

    bits[16] = floatNum.bits.a17;
    bits[17] = floatNum.bits.a18;
    bits[18] = floatNum.bits.a19;
    bits[19] = floatNum.bits.a20;
    bits[20] = floatNum.bits.a21;
    bits[21] = floatNum.bits.a22;
    bits[22] = floatNum.bits.a23;
    bits[23] = floatNum.bits.a24;

    bits[24] = floatNum.bits.a25;
    bits[25] = floatNum.bits.a26;
    bits[26] = floatNum.bits.a27;
    bits[27] = floatNum.bits.a28;
    bits[28] = floatNum.bits.a29;
    bits[29] = floatNum.bits.a30;
    bits[30] = floatNum.bits.a31;
    bits[31] = floatNum.bits.a32;

    for (int i = 0; i < 16; i++) {
        bool tmp = bits[i];
        bits[i] = bits[31 - i];
        bits[31 - i] = tmp;
    }

    std::cout << "Bits structure:";
    for (int i = 0; i < 32; i++) {
        if (i % 4 == 0)
            std::cout << " ";
        std::cout << bits[i];
    }
    std::cout << "\n";
    printf("Byte Structure: %X\n", floatNum.uValue);

    std::string sign = (floatNum.stValue.sign == 0 ? "+" : "-");
    std::string info;
    int exponent = 0x7f;
    int infoNumber = 0;
    for (int i = 1; i < 9; i++) {
        if (i % 4 == 1)
            info += " ";
        info += '0' + bits[i];
        infoNumber = infoNumber << 1;
        infoNumber += bits[i];
    }
    exponent = infoNumber - exponent;
    std::string mantis;
    for (int i = 9; i < 32; i++) {
        if (i % 4 == 1)
            mantis += " ";
        mantis += '0' + bits[i];
    }

    std::cout << "sign: " << sign << "\n";
    std::cout << "info:" << info << "\n";
    std::cout << "mantis:" << mantis << "\n";
    std::cout << "exponent: " << exponent << "\n";
    std::cout << "Size of my structure: " << sizeof(floatNum) << " bytes\n";
}
