﻿#include <iostream>

const signed char Five = 5;
const signed char HundredTwentySeven = 127;
const signed char Two = 2;
const signed char Three = 3; // 3
const signed char MinusHundredTwenty = -120; // -120
const signed char MinusThirtyFour = -34; // -34
const signed char MinusFive = -5; // -5
const signed char FiftySix = 56; // 56
const signed char ThirtyEight = 38; // 38

int main()
{
    signed char result;

    result = Five + HundredTwentySeven;
    std::cout << "5+127= " << (int)(result) <<"\n";

    result = Two - Three;
    std::cout << "2-3= " << (int)(result) << "\n";

    result = MinusHundredTwenty + MinusThirtyFour;
    std::cout << "-120-34= " << (int)(result) << "\n";

    int res = (unsigned char)(MinusFive);
    std::cout << "unsigned char(-5)= " << res << "\n";

    result = FiftySix & ThirtyEight;
    std::cout << "56&38= " << (int)(result) << "\n";

    result = FiftySix | ThirtyEight;
    std::cout << "56|38= " << (int)(result) << "\n";
}