﻿#include <iostream>
#include <time.h>

#define YEARBITSSIZE 6
#define MONTHBITSSIZE 4
#define DAYBITSSIZE 5
#define HOURBITSSIZE 5
#define MINUTEBITSSIZE 6
#define SECONDBITSSIZE 6

struct DateTime 
{
    uint32_t year : YEARBITSSIZE;
    uint32_t month : MONTHBITSSIZE;
    uint32_t day : DAYBITSSIZE;

    uint32_t hour : HOURBITSSIZE;
    uint32_t minute : MINUTEBITSSIZE;
    uint32_t second : SECONDBITSSIZE;
};

int main()
{
    DateTime t{ 24, 2, 22, 11, 46, 26};

    std::cout << "Size of default structure is " << sizeof(tm) << " bytes\n";
    std::cout << "Size of defined structure is " << sizeof(DateTime) << " bytes\n";
    std::cout << t.day << "." << t.month << "." << t.year << "  " << t.hour << ":" << t.minute << ":" << t.second << "\n";
}
