﻿#include <iostream>
#include <chrono>
#define n 150
#define K 20000

class Random {
private:
    const uint64_t a = 1664525, c = 1013904223, m = 0x100000000;
    uint64_t m_seed;
public:
    Random(uint64_t seed) : m_seed(seed) { }
    uint64_t Next() {
        m_seed = (a * m_seed + c) % m;
        return m_seed;
    }
};

int main()
{
    system("chcp 1251"); system("cls");
    Random random(std::chrono::steady_clock::now().time_since_epoch().count());

    int frequency[n] {};
    double probability[n] {};

    for (int i = 0; i < K; i++) {
        frequency[random.Next()%n]++;
    }
    for (int i = 0; i < n; i++) {
        probability[i] = (double)frequency[i] / K;
    }
    double m = 0;
    for (int i = 0; i < n; i++) {
        m += i * probability[i];
    }
    
    double d = 0;
    for (int i = 1; i <= n; ++i)
        d += pow(frequency[i - 1] - m, 2) * probability[i - 1];

    for (int i = 0; i < n; i++) {
        std::cout << i << ", Частота: " << frequency[i] << ", Ймовірність: " << probability[i] << "\n";
    }
    double delta = sqrt(d);

    std::cout << "Сподівання = " << m << "\n";
    std::cout << "Дисперсія = " << d << "\n";
    std::cout << "Середньо-квадратичне відхилення =" << delta << "\n";
}