﻿#include <iostream>
#include <vector>
#include <chrono>

using namespace std;

vector<float> sortTimes;
vector<float> sizes;
vector<double> numbers;

void seave_down(int n, int size) {
    int max = n;
    int left = 2 * n + 1;
    int right = 2 * n + 2;
    if (left < size && numbers[left] > numbers[max]) {
        max = left;
    }
    if (right < size && numbers[right] > numbers[max]) {
        max = right;
    }
    if (max != n) {
        double tmp = numbers[max];
        numbers[max] = numbers[n];
        numbers[n] = tmp;
        seave_down(max, size);
    }
}
void sort(int size) {
    for (int i = size / 2 - 1; i >= 0; i--) {
        seave_down(i, size);
    }
    for (int i = size - 1; i >= 0; i--) {
        double tmp = numbers[i];
        numbers[i] = numbers[0];
        numbers[0] = tmp;
        seave_down(0, i);
    }
}
void randomise(int size) {
    numbers.clear();
    for (int i = 0; i < size; i++)
    {
        numbers.push_back(rand() % 11001 / (double)100 - 10);
    }
}


int main()
{
    sizes.push_back(10);
    sizes.push_back(100);
    sizes.push_back(500);
    sizes.push_back(1000);
    sizes.push_back(2000);
    sizes.push_back(5000);
    sizes.push_back(10000);

    for (int size : sizes)
    {
        randomise(size);
        auto start = chrono::high_resolution_clock::now();
        sort(size);
        auto end = chrono::high_resolution_clock::now();
        sortTimes.push_back(chrono::duration_cast<chrono::microseconds>(end - start).count());
    }
    std::cout << "Sort times\n";
    for (int i = 0; i < sizes.size(); i++)
    {
        cout << sortTimes[i] << "\n";
    }
    
}
