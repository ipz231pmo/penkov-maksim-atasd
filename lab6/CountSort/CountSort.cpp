﻿#include <iostream>
#include <vector>
#include <chrono>
using namespace std;
vector<float> sortTimes;
vector<int> sizes;
vector<char> numbers;
void sort(int size) {
    vector<int> counts(91);
    for (int i = 0; i < size; i++)
    {
        counts[numbers[i] + 100]++;
    }
    for (int i = 0, c = 0; i < 90; i++) {
        for (int j = 0; j < counts[i]; j++) {
            numbers[c++] = i - 100;
        }
    }
}
void randomise(int size) {
    numbers.clear();
    for (int i = 0; i < size; i++)
    {
        numbers.push_back(rand() % 91 - 100);
    }
}
int main()
{
    sizes.push_back(10);
    sizes.push_back(100);
    sizes.push_back(500);
    sizes.push_back(1000);
    sizes.push_back(2000);
    sizes.push_back(5000);
    sizes.push_back(10000);
    for (int size : sizes)
    {
        randomise(size);
        auto start = chrono::high_resolution_clock::now();
        sort(size);
        auto end = chrono::high_resolution_clock::now();
        sortTimes.push_back(chrono::duration_cast<chrono::microseconds>(end - start).count());
    }
    std::cout << "Sort times\n";
    for (int i = 0; i < sizes.size(); i++)
    {
        cout << sortTimes[i] << "\n";
    }
}