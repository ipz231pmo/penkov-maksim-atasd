﻿#include <iostream>
#include <vector>
#include <chrono>
using namespace std;
#define SIZE 100
vector<float> numbers;
vector<float> sortTimes;
vector<int> sizes;
vector<int> gaps;
void sort(int size) {
    for (auto it = gaps.rbegin(); it != gaps.rend(); it++) {
        int gap = *it;
        for (int i = gap; i < size; i++) {
            float tmp = numbers[i];
            int j;
            for (j = i; j >= gap && numbers[j - gap] > tmp; j -= gap) {
                numbers[j] = numbers[j - gap];
            }
            numbers[j] = tmp;
        }
    }
}
void calcGaps(int size) {
    gaps.clear();
    for (int i = 1; ; i++)
    {
        int gap = (pow(3, i) - 1) / 2;
        if (gap >= size) break;
        gaps.push_back(gap);
    }
}
void randomise(int size) {
    numbers.clear();
    for (int i = 0; i < size; i++) {
        numbers.push_back(rand() % 20001 / (float)100);
    }
}
int main()
{
    sizes.push_back(10);
    sizes.push_back(100);
    sizes.push_back(500);
    sizes.push_back(1000);
    sizes.push_back(2000);
    sizes.push_back(5000);
    sizes.push_back(10000);
    for(int size:sizes)
    {
        randomise(size);
        calcGaps(size);
        auto start = chrono::high_resolution_clock::now();
        sort(size);
        auto end = chrono::high_resolution_clock::now();
        sortTimes.push_back(chrono::duration_cast<chrono::microseconds>(end-start).count());
    }
    std::cout << "Sort times\n";
    for (int i = 0; i < sizes.size(); i++)
    {
        cout << sortTimes[i] << "\n";
    }
}