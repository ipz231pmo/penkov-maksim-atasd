﻿#include <iostream>

using namespace std;

template <class T>
class LinkedList {
private:
	struct Node
	{
		T data;
		Node* next;
		Node* prev;
	};
	Node *head, *end;
public:
	static struct iterator {

		using value_type = T;
		using pointer = Node*;
		using difference_type = std::ptrdiff_t;
		using reference = T&;

		iterator() : m_ptr(nullptr) {}
		iterator(pointer ptr) : m_ptr(ptr) {}

		reference operator*() const { return m_ptr->data; }

		iterator& operator++() {
			m_ptr = m_ptr->next;
			return this;
		}
		iterator& operator++(int) {
			iterator tmp = *this;
			m_ptr = m_ptr->next;
			return tmp;
		}
		iterator& operator--() {
			m_ptr = m_ptr->prev;
			return this;
		}
		iterator& operator--(int) {
			iterator tmp = *this;
			m_ptr = m_ptr->prev;
			return tmp;
		}

		friend bool operator !=(const iterator& left, const iterator& right) {
			return left.m_ptr != right.m_ptr;
		}
		friend bool operator ==(const iterator& left, const iterator& right) {
			return left.m_ptr == right.m_ptr;
		}
		
		iterator& operator +=(difference_type n) {
			for (int i = 0; i < n; i++) {
				m_ptr = m_ptr->next;
			}
			return *this;
		}
		iterator& operator -=(difference_type n) {
			for (int i = 0; i < n; i++) {
				m_ptr = m_ptr->prev;
			}
			return *this;
		}
		friend iterator& operator +(const iterator& it, difference_type n) {
			iterator tmp = it;
			tmp += n;
			return tmp;
		}
		friend iterator& operator +(difference_type n, const iterator& it) {
			return it + n;
		}
		friend iterator& operator -(const iterator& it, difference_type n) {
			iterator tmp = it;
			tmp -= n;
			return tmp;
		}
	private:
		pointer m_ptr;
	};
	iterator it_begin() {
		return iterator(head);
	}
	iterator it_end() {
		return iterator(nullptr);
	}
	void push_front(T data) {
		Node *node = new Node { data, head, nullptr };
		if (head != nullptr) head->prev = node;
		head = node;
		if (end == nullptr) end = node;
	}
	void push_back(T data) {
		Node* node = new Node{ data, nullptr, end };
		if (end != nullptr) end->next = node;
		end = node;
		if (head == nullptr) head = node;
	}
	LinkedList() {
		head = nullptr;
		end = nullptr;
	}
	void insert(T _data, int _pos) {
		if (_pos == 0) {
			push_front(_data);
			return;
		}
		Node *cur = head;
		for (int i = 0; i < _pos; i++) {
			if (cur->next == nullptr) {
				push_back(_data);
				return;
			}
			cur = cur->next;
		}

		Node *prev = cur->prev;
		Node* next = cur;

		Node* node = new Node{_data, next, prev };

		if (prev != nullptr) prev->next = node;
		if (next != nullptr) next->prev = node;			
	}

	T operator [](int n) {
		int s = 0;
		Node* iterator = head;
		if (n == 0) return iterator->data;
		for (int i = 0; i < n; i++)
		{
			iterator = iterator->next;
		}
		return iterator->data;
	}
	int size() {
		if (head == nullptr) return 0;
		Node* cur = head;

		int s = 1;
		while (cur->next != nullptr)
		{
			s++;
			cur = cur->next;
		}
		return s;
	}
	void pop_back() {
		if (end == nullptr) return;
		Node *cur = end;
		end = end->prev;
		if (end != nullptr) end->next = nullptr;
		delete cur;
	}
	void pop_inside(int n) {
		if (n == 0) {
			pop_front();
			return;
		}
		if (n == size() - 1)
		{
			pop_back();
			return;
		}

		Node* cur = head;
		for (int s = 0; s < n; s++) {
			cur = cur->next;
		}
		cur->prev->next = cur->next;
		cur->next->prev = cur->prev;
		delete cur;
	}
	void pop_front() {
		if (head == nullptr) return;		
		Node *cur = head;		
		head = head->next;
		if (head != nullptr) head->prev = nullptr;
		delete cur;		
	}

	~LinkedList() {
		Node* next = nullptr;
		while (head != nullptr)
		{
			next = head->next;
			delete head;
			head = next;
		}
	}
};

int main()
{
	std::cout << "Hello World!\n";
	LinkedList<int> list;

	list.push_back(11);
	list.push_back(5);
	list.push_back(19);
	list.push_back(2);

	list.pop_inside(1);

	auto it1 = list.it_begin();
	auto it2 = list.it_begin();
	it2 += 2;
	std::cout << "Iter: " << *it2 << "\n";

	for (auto it = list.it_begin(); it != list.it_end(); it++) {
		std::cout << "Iterator value: " << *it << "\n"; // 11 19 2
	}
}