﻿#include <stack>
#include <string>
#include <iostream>
#include <cmath>
#include <vector>
std::stack<double> st;
enum operators {
    plus,
    minus,
    multiply,
    divide,
    sqrRoot,
    power
};
operators CurrentOperator;

void process_lexem(std::string tmp) {
    bool IsNumber = true;
    for (int i = 0; i < tmp.size(); i++) 
    {
        if (tmp[i] - '0' >= 0 && tmp[i] - '0' <= 9 || tmp[i] == '.')
            continue;
        else {
            IsNumber = false;
            break;
        }
    }

    if (IsNumber) st.push(std::stod(tmp));
    else {
        if (tmp == "sqrt") CurrentOperator = sqrRoot;
        else if (tmp == "+") CurrentOperator = plus;
        else if (tmp == "-") CurrentOperator = minus;
        else if (tmp == "/") CurrentOperator = divide;
        else if (tmp == "*") CurrentOperator = multiply;
        else if (tmp == "^") CurrentOperator = power;
        else throw "Scan error";

        double cur = st.top();
        st.pop();
        double prev;
        if (CurrentOperator != sqrRoot)
        {
            prev = st.top();
            st.pop();
        }

        switch (CurrentOperator){
        case plus:
            st.push(prev + cur);
            break;
        case minus:
            st.push(prev - cur);
            break;
        case multiply:
            st.push(prev * cur);
            break;
        case divide:
            if (cur == 0) throw "Zero div";
            st.push(prev / cur);
            break;
        case sqrRoot:
            if (cur < 0) throw "Minus sqrt";
            st.push(std::sqrt(cur));
            break;
        case power:
            st.push(std::pow(prev, cur));
            break;
        default:
            throw "Unknown operator";
            break;
        }
    }
}
int main()
{
    while (true)
    {
        std::cout << "\nEnter statement in polish reverse notation.\nPres Cntrl + C to exit\n";
        std::string str, word;
        std::vector<std::string> strings;
        std::getline(std::cin, str);
    
        for (auto ch : str)
            if (ch == ' ')
            {
                strings.push_back(word);
                word = "";
            }
            else {
                word += ch;
            }
        if (str[str.size() - 1] != ' ') strings.push_back(word);
        for (std::string s : strings)
            process_lexem(s);
        std::cout << "Res: " << st.top() << "\n";
    }
}