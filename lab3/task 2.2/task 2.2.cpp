﻿#include <iostream>
#include <chrono>
#define TEST_COUNTS 10000
unsigned long long fact(unsigned long long n) {
    if (n == 0) return 1;
    return n * fact(n - 1);
}
int main()
{
    int times[21] = {};
    unsigned long long res;
    for (int i = 0; i <= 20; i++)
    {
        auto begin = std::chrono::steady_clock::now();
        for(int j = 0; j < TEST_COUNTS; j++)
            res = fact(i);
        auto end = std::chrono::steady_clock::now();
        std::cout << "Value: " << res << "\n";
        times[i] = std::chrono::duration_cast<std::chrono::nanoseconds> (end-begin).count()/TEST_COUNTS;
    }
    std::cout << "\n\nTimes:\n";
    for (int i = 0; i <= 20; i++) {
        std::cout << times[i] << "\n";
    }
}