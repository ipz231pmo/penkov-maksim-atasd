﻿#include <iostream>
#include <vector>
#include <chrono>
#define TEST_COUNTS 100000 

std::vector<int> Array;

void solution(int c) {
	int nums[8] = {};
	for (int i = 0; i < Array.size(); i++) {
		nums[Array[i]]++;
	}
	std::string res;
	for (int i = 7; i >= 0; i--)
	{
		while(nums[i])
			if (nums[i]) {
				res += '0' + i;
				nums[i]--;
			}
	}
#ifdef _DEBUG
	if(!c)
		std::cout << "Biggest num: " << res << "\n";
#endif // !_DEBUG

}

int main()
{
	srand(time(NULL));

	for (int i = 1; i <= 20; i++)
	{
		Array.clear();
		Array.resize(i);

#ifdef _DEBUG
		std::cout << "array = { ";
		for (int j = 0; j < Array.size(); j++)
		{
			Array[j] = rand()%8;
			std::cout << Array[j] << " ";
		}
		std::cout << "}\n";
#endif // !_DEBUG
		
		auto begin = std::chrono::steady_clock::now();
		
		for(int c = 0; c < TEST_COUNTS; c++)
			solution(c);

		auto end = std::chrono::steady_clock::now();

		int TimeElapsed = std::chrono::duration_cast<std::chrono::nanoseconds> (end - begin).count();

#ifdef _DEBUG
		std::cout << "Array size: " << Array.size() << " Time: " << TimeElapsed / TEST_COUNTS << "\n\n\n";
#else
		std::cout << TimeElapsed / TEST_COUNTS << "\n";
#endif // !_DEBUG

	}
}
