﻿#include <iostream>

typedef double (*func)(int);
void tab(func f) {
    for (int i = 0; i <= 50; i++) {
        double res = f(i);
        std::cout << (res > 500 ? 500 : res) << "\n";
    }
}

double LinearFunc(int x) {
    return x;
}
double LogFunc(int x) {
    return log2(x);
}
double LinearLogFunc(int x) {
    return x*log2(x);
}
double QuadraticFunc(int x) {
    return x*x;
}
double ExpFunc(int x) {
    return pow(2, x);
}
double FactoriaFunc(int x) {
    if (x == 0) return 1;
    return x * FactoriaFunc(x-1);
}


int main()
{
    std::cout << "Linear Func: \n";
    tab(LinearFunc);
    std::cout << "\n\n\n";

    std::cout << "Log Func: \n";
    tab(LogFunc);
    std::cout << "\n\n\n";

    std::cout << "Linear Log Func: \n";
    tab(LinearLogFunc);
    std::cout << "\n\n\n";

    std::cout << "Quadratic Func: \n";
    tab(QuadraticFunc);
    std::cout << "\n\n\n";

    std::cout << "Exp Func: \n";
    tab(ExpFunc);
    std::cout << "\n\n\n";

    std::cout << "Factorial Func: \n";
    tab(FactoriaFunc);
    std::cout << "\n\n\n";
}
